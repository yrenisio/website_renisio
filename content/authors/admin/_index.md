---
authors:
- admin
bio: My research themes include sociology of aspirations, socialization of elites, family, health and higher education
  matter.
education:
  courses:
  - course: PhD in Sociology
    institution: École des Hautes Études en Sciences Sociales (EHESS)
    year: 2017
  - course: MA in Social Sciences
    institution: École Normale Supérieure & EHESS
    year: 2012
  - course: BA in Political Science
    institution: Sciences Po Lille & Ottawa University
    year: 2010
email: "yann.renisio@protonmail.com"
interests:
- Sociology
- Data science
- R
name: Yann Renisio
organizations:
- name: Uppsala University
  url: ""
role: Postdoc
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/GeorgeCushen
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.fr/citations?user=TJb1SNMAAAAJ
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/yrenisio
superuser: true
user_groups:
- Researchers
- Visitors
---

I am a postdoc in sociology at Uppsala University (departement of sociology of education and culture). My research interests could be summarized into "why is life unfair", with "unfair" being defined as an over-representation of particular groups with significantly more or less material or symbolic resources than others, and "why" being mostly determinants of aspirations and practices. I have worked on upper-secondary and higher education students' trajectories, on academics, mathematicians and recently on physicians. I am very interested in the conjugated use of ethnographic and statistical analysis, but my research questions (Why are social sciences considered as "not really science"?; Who becomes mathematician/physician? ; What makes people (not) apply where they could (not) have succeeded?) led me to focus more on the latter part so far.



