---
title: Let's test a new post!
author: Yann
date: '2019-12-09'
slug: let-s-test-a-new-post
categories: []
tags: []
subtitle: 'Falsifying the Habitus Concept'
summary: ''
authors: []
lastmod: '2019-12-09T16:23:38+01:00'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

I am working right now with 3 swedish colleagues (A. ...) on a paper 



```r
plot(cars)
```

<img src="/post/2019-12-09-let-s-test-a-new-post_files/figure-html/unnamed-chunk-1-1.png" width="672" />


```r
library(tidyverse)
```

```
## ── Attaching packages ────────────────────────────────────────────────────────────────────────────────────────── tidyverse 1.3.0 ──
```

```
## ✔ ggplot2 3.2.1     ✔ purrr   0.3.3
## ✔ tibble  2.1.3     ✔ dplyr   0.8.3
## ✔ tidyr   1.0.0     ✔ stringr 1.4.0
## ✔ readr   1.3.1     ✔ forcats 0.4.0
```

```
## ── Conflicts ───────────────────────────────────────────────────────────────────────────────────────────── tidyverse_conflicts() ──
## ✖ dplyr::filter() masks stats::filter()
## ✖ dplyr::lag()    masks stats::lag()
```

```r
ggplot(cars, aes(x = speed, y = dist))+
  geom_point()+
  geom_smooth(se = FALSE)+
  theme_bw()
```

```
## `geom_smooth()` using method = 'loess' and formula 'y ~ x'
```

<img src="/post/2019-12-09-let-s-test-a-new-post_files/figure-html/unnamed-chunk-2-1.png" width="672" />


