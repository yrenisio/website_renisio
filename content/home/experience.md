+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 120  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Postdoc"
  company = "Uppsala University"
  company_url = ""
  location = "Sweden"
  date_start = "2019-03-01"
  date_end = ""
  description = """
  Responsibilities include:
  
  * Analysis of Swedish data
  * Teaching R and data science to academics and phd students
  * Publishing articles
  """

[[experience]]
  title = "Postdoc"
  company = "Collège de France"
  company_url = ""
  location = "Paris"
  date_start = "2017-06-28"
  date_end = "2019-02-28"
  description = """
  Responsibilities include:
  
  * Complex merging of files on academic careers and productions
  * Organizing international conferences
  * Publishing articles
  """
  
  [[experience]]
  title = "Research assistant"
  company = "Collège de France"
  company_url = ""
  location = "Paris"
  date_start = "2015-09-01"
  date_end = "2017-06-28"
  description = """
  Responsibilities include:
  
  * Analysis on upper-secondary teachers working in Higher education
  * Organizing international conferences
  * Publishing articles
  """
  
+++
